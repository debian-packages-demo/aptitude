# aptitude

terminal-based package manager
* https://tracker.debian.org/pkg/aptitude

# Semi-official documentation
* [*Comparison with other distros*
  ](https://wiki.alpinelinux.org/wiki/Comparison_with_other_distros)

# License: GNU General Public License v2.0
* https://salsa.debian.org/apt-team/aptitude/blob/debian-sid/COPYING
* https://sources.debian.org/src/aptitude/0.8.10-9/COPYING/
